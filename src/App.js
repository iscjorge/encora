import SearchDropDown from './components/SearchDropDown';

function App() {
  return (
    <div className="App">
      <SearchDropDown />
    </div>
  );
}

export default App;
