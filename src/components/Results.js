/* eslint-disable eqeqeq */
import { useEffect } from 'react'
import ResultCard from './ResultCard'

const INPUTID = 'search-input'

function Results({ searchResults }) {

    const downHandler = (e) => {
        let active = searchResults.findIndex(result => result.id == document.activeElement.id)
        const inputField =  document.getElementById(INPUTID)

        if (e.keyCode == '38') {
            if (document.activeElement.id != INPUTID && searchResults.length > 0) {
                if (searchResults[active - 1]){
                    document.getElementById(searchResults[active - 1].id).focus()
                } else {
                    inputField.focus()
                }
            }
        }
        else if (e.keyCode == '40') {
            if (document.activeElement.id == INPUTID && searchResults.length > 0) {
                document.getElementById(searchResults[0].id).focus()
            } else {
                if (searchResults[active + 1]) {
                    document.getElementById(searchResults[active + 1].id).focus()
                } else {
                    document.getElementById(searchResults[0].id).focus()
                }
            }
        }
    }

    useEffect(() => {
        window.addEventListener("keydown", downHandler);
        return () => {
            window.removeEventListener("keydown", downHandler);
        };
    });

    return (
        <>
            {
                searchResults.map(result => <ResultCard {...result} key={result.id}></ResultCard>)
            }
        </>
    )
}

export default Results
