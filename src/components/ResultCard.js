import style from './ResultCard.module.css'

function ResultCard({ id, title, html_url }) {
    return (
        <div className={style.card}>
            <a className={style.links} id={id} href={html_url} target="_blank" rel="noreferrer">{title}</a>
        </div>
    )
}

export default ResultCard
