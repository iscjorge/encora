import { useState } from 'react'
import Results from './Results'
import style from './SearchDropDown.module.css'

const API = 'https://api.github.com/search/issues?q='
const OPTIONS = '+in:title+user:facebook+repo:react'

function SearchDropDown() {

    const [searchResults, setSearchResults] = useState([])
    const [loading, setLoading] = useState(false)

    const handleSearch = async (e) => {
        setLoading(true)
        try {
            const response = await fetch(API + e.target.value + OPTIONS)
            // eslint-disable-next-line eqeqeq
            if (response.status == '403') { alert('Github error 403') }
            else {
                const data = await response.json();
                data.items && data.items.length > 0 ? setSearchResults(data.items) : setSearchResults([])
            }
        } catch (error) {
            alert(error)
        }
        setLoading(false)
    }
    return (
        <div className={style.container}>
            <input id="search-input" type='text' onChange={handleSearch} className={style.searchResults} />
            <div id="searchResults">
                {
                    loading ? 'Loading...' : <Results searchResults={searchResults}></Results>
                }
            </div>
        </div>
    )
}

export default SearchDropDown
